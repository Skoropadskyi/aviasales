//Activity
let selected;

document.querySelector('.language').addEventListener("click", event => {
    let target = event.target;

    if (selected) {
        selected.classList.remove('active');
    }
    selected = target;
    selected.classList.add('active');
});

// Currency
const prices = [
    3968,
    5153,
    6947,
    5151,
    8147,
    4064,
    5546,
    2999,
    9660,
    15114
];

function updatePrices(value) {
    const elements = document.getElementsByClassName("price");

    prices.forEach((price, index) => {
        elements[index].innerHTML = (price * value).toFixed(2)
    });
}

updatePrices(1);

// Filter
const filterBox = document.querySelectorAll('.ticket');

document.querySelector('.filter-transfer').addEventListener('click', event => {
    if(event.target.tagName !== 'INPUT') return false;

    let filterClass = event.target.dataset['f'];
    console.log(filterClass);

    filterBox.forEach(elem => {
        elem.classList.remove('hide');
        if (!elem.classList.contains(filterClass) && filterClass !=='all') {
            elem.classList.add('hide');
        }
    });
});
